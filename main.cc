#include <cassert>
#include <vector>
#include "BTree/Core/BTreeTemplate.h"
#include "Util/ConfigHelper.h"
#include "Util/StringUtil.h"
#include "DTranx/Util/Log.h"

using namespace BTree;
typedef Core::BTreeTemplate<uint64_t, 264> BTreeInt;

//TODO: compile proto file so that no calling to Deploy script is required before compile
//because now protoc generate header files at Builder directory instead of include directory


int main(int argc, char ** argv) {
	DTranx::Util::Log::setLogPolicy( { { "Client", "PROFILE" }, { "Server", "PROFILE" }, { "Tranx",
			"PROFILE" }, { "Storage", "PROFILE" }, { "RPC", "PROFILE" }, { "Util", "PROFILE" }, {
			"Log", "PROFILE" } });
	if (argc != 3) {
		std::cout << "arg1: self address to bind to; arg2: self port to bind to" << std::endl;
		exit(1);
	}
	std::string selfAddress(argv[1]);
	std::string selfPort(argv[2]);
	Util::ConfigHelper configHelper;
	configHelper.readFile("BTree.conf");
	std::string ipsStr = configHelper.read("IPS");
	std::vector<std::string> ips = Util::StringUtil::Split(ipsStr, ';');
	bool enableDebug = ("1" == configHelper.read("EnableDebug"));

	BTreeInt btreeInt(enableDebug, "btree.debug");
	btreeInt.InitDTranxForBTree(30000, ips, selfAddress, std::stoul(selfPort.c_str(), nullptr, 10),
	true, false, false);

	uint64_t insertStart = std::stoull(configHelper.read("InsertStart"));
	uint64_t insertEnd = std::stoull(configHelper.read("InsertEnd"));
	uint64_t searchStart = std::stoull(configHelper.read("SearchStart"));
	uint64_t searchEnd = std::stoull(configHelper.read("SearchEnd"));
	uint64_t eraseStart = std::stoull(configHelper.read("EraseStart"));
	uint64_t eraseEnd = std::stoull(configHelper.read("EraseEnd"));

	for (int i = insertStart; i < insertEnd; ++i) {
		if (i % 10 == 0) {
			std::cout << "insert " << i << std::endl;
		}
		btreeInt.insert_unique(i);
	}

	for (int i = searchStart; i < searchEnd; ++i) {
		if (i % 10 == 0) {
			std::cout << "find " << i << std::endl;
		}
		btreeInt.find_unique(i);
	}

	for (int i = eraseStart; i < eraseEnd; ++i) {
		if (i % 10 == 0) {
			std::cout << "erase " << i << std::endl;
		}
		btreeInt.erase_unique(i);
	}
	return 0;
}
