/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <cassert>
#include "BTree/Util/DebugLog.h"

namespace BTree {
namespace Util {

DebugLog::DebugLog(bool enableDebug, std::string fileName)
		: enableDebug(enableDebug), fileName(fileName) {
	if (enableDebug) {
		file_ = fopen(fileName.c_str(), "w");
		assert(file_ != NULL);
	}
}

DebugLog::~DebugLog() {
	if (file_) {
		assert(fclose(file_) == 0);
		file_ = NULL;
	}
}

void DebugLog::Write(std::string data) {
	if (enableDebug) {
		size_t size = fwrite_unlocked(data.c_str(), 1, data.size(), file_);
		assert(size == data.size());
	}
}

void DebugLog::Flush() {
	if (enableDebug) {
		fflush_unlocked(file_);
	}
}

}
}
