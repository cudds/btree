/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */
#include "BTree/Util/DTranxKeySpace.h"
namespace BTree {
namespace Util {
std::string DTranxKeySpace::NOSUCHNODE = "0";
std::string DTranxKeySpace::METANODEKEY = "metadata";
std::string DTranxKeySpace::ROOTNODEKEY = "1";
char DTranxKeySpace::DELIMITER = '-';

DTranxKeySpace::DTranxKeySpace(uint64_t clientID)
		: clientID_(clientID) {
	nextID_ = 1;
}

std::string DTranxKeySpace::GetNextNewKey() {
	return std::to_string(clientID_) + DELIMITER + std::to_string(nextID_++);
}

}
}
