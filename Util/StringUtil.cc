/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "StringUtil.h"
#include <cstdarg>
#include <cassert>
#include <sstream>

namespace BTree {
namespace Util {

StringUtil::StringUtil() {

}

StringUtil::~StringUtil() {
}

std::vector<std::string> StringUtil::Split(std::string str, char delimiter) {
	/*
	 std::vector<std::string> internal;
	 std::stringstream ss(str); // Turn the string into a stream.
	 std::string tok;

	 while(getline(ss, tok, delimiter)) {
	 internal.push_back(tok);
	 }
	 if( internal.size()>0 && internal.back().empty()){
	 internal.pop_back();
	 }
	 return internal;
	 */
	size_t start = 0;
	size_t end = str.find_first_of(delimiter);
	std::vector<std::string> result;
	size_t size = str.size();
	if (size == 0) {
		return result;
	}
	while (end <= std::string::npos) {
		result.emplace_back(str.substr(start, end - start));
		if (end == std::string::npos || end == size - 1) {
			break;
		}
		start = end + 1;
		end = str.find_first_of(delimiter, start);
	}
	return result;
}

void StringUtil::Split(std::string str, char delimiter, std::vector<std::string>& result) {
	size_t start = 0;
	size_t end = str.find_first_of(delimiter);
	size_t size = str.size();
	if (size == 0) {
		return;
	}
	while (end <= std::string::npos) {
		result.emplace_back(str.substr(start, end - start));
		if (end == std::string::npos || end == size - 1) {
			break;
		}
		start = end + 1;
		end = str.find_first_of(delimiter, start);
	}
	return;
}

} /* namespace Util */
} /* namespace BTree */
