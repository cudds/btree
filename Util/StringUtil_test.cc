/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "StringUtil.h"

using namespace BTree;
TEST(StringUtil, Split){
	std::string srcStr= "1#12#123";
	char delimiter = '#';
	std::vector<std::string> desStrs = Util::StringUtil::Split(srcStr, delimiter);
	EXPECT_EQ(3, desStrs.size());
	EXPECT_EQ("12", desStrs[1]);
	srcStr= "1#12#123#";
	desStrs = Util::StringUtil::Split(srcStr, delimiter);
	EXPECT_EQ(3, desStrs.size());
	EXPECT_EQ("123", desStrs[2]);
	srcStr= "";
	desStrs = Util::StringUtil::Split(srcStr, delimiter);
	EXPECT_EQ(0, desStrs.size());
	srcStr= "123#";
	desStrs = Util::StringUtil::Split(srcStr, delimiter);
	EXPECT_EQ(1, desStrs.size());
}
