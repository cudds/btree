#include "BTree/Util/DTranxHelper.h"
#include "BTree/Util/DTranxKeySpace.h"

namespace BTree {
namespace Util {

DTranxHelper::DTranxHelper(uint32_t routerFrontPort, std::vector<std::string> ips,
		std::string selfAddress, uint32_t selfPort,
		bool clientCacheEnabled, bool snapshotTranx)
		: snapshotTranx(snapshotTranx) {
	std::cout << "DtranxHelper constructor" << std::endl;
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(1);
	clientTranx = new DTranx::Client::ClientTranx(routerFrontPort, context, ips, selfAddress,
			selfPort, clientCacheEnabled,
			DTranx::Client::ClientTranx::CoordinatorStrategy::ONEPHASECOMMIT);
	clientTranx->SetThreadsafe();
}

DTranxHelper::~DTranxHelper() {
	if (clientTranx) {
		delete clientTranx;
	}
}

void DTranxHelper::InitClient(std::string ip, DTranx::Client::Client* client) {
	clientTranx->InitClients(ip, client);
}

std::string DTranxHelper::ReadNode(std::string nodeID) {
	std::string value;
	DTranx::Service::GStatus status = clientTranx->Read(const_cast<std::string&>(nodeID), value, snapshotTranx);
	if (status == DTranx::Service::GStatus::OK) {
		return value;
	} else {
		//std::cout << "reading " << (nodeID) << " failure" << std::endl;
		return DTranxKeySpace::NOSUCHNODE;
	}
}

uint64_t DTranxHelper::ReadMeta() {
	std::string value;
	int retryTimes = 1;
	while (true) {
		DTranx::Service::GStatus status = clientTranx->Read(
				const_cast<std::string&>(DTranxKeySpace::METANODEKEY), value);
		if (status == DTranx::Service::GStatus::OK) {
			std::string nextClientID = std::to_string(std::strtoull(value.c_str(), NULL, 10) + 1);
			clientTranx->Write(DTranxKeySpace::METANODEKEY, nextClientID);
			if (clientTranx->Commit()) {
				clientTranx->Clear();
				std::cout << "success to read metadata" << std::endl;
				break;
			}
			retryTimes++;
			std::cout << "success to read metadata, but not able to commit, retrying" << std::endl;
			clientTranx->Clear();
		} else {
			clientTranx->Clear();
			std::cout << "no metadata in database, trying to create one" << std::endl;
			std::string initialNextClientID = "2";
			clientTranx->Write(DTranxKeySpace::METANODEKEY, initialNextClientID);
			if (clientTranx->Commit()) {
				std::cout << "new metadata created" << std::endl;
				clientTranx->Clear();
				value = "1";
				break;
			}
			std::cout << "new metadata failed, retrying" << std::endl;
			retryTimes++;
			clientTranx->Clear();
		}
	}
	std::cout << "metadata transaction retried for " << retryTimes << " times and clientid is "
			<< value << std::endl;
	return std::strtoull(value.c_str(), NULL, 10);
}

void DTranxHelper::WriteNode(std::string nodeID, std::string content) {
	clientTranx->Write(nodeID, content);
}

bool DTranxHelper::Commit() {
	bool success = clientTranx->Commit();
	clientTranx->Clear();
	return success;
}

void DTranxHelper::Abort() {
	clientTranx->Abort();
}

void DTranxHelper::InitMeta() {
	std::string value;
	DTranx::Service::GStatus status = clientTranx->Read(
			const_cast<std::string&>(DTranxKeySpace::METANODEKEY), value);
	assert(status != DTranx::Service::GStatus::OK);
	clientTranx->Clear();
	std::cout << "initializing clientid(note: this should be called when btree is first created)"
			<< std::endl;
	std::string initialNextClientID = "1";
	clientTranx->Write(DTranxKeySpace::METANODEKEY, initialNextClientID);
	assert(clientTranx->Commit());
	clientTranx->Clear();
}

}  // namespace Util
}  // namespace BTree
