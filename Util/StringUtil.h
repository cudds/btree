/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef BTREE_UTIL_STRINGUTIL_H_
#define BTREE_UTIL_STRINGUTIL_H_
#include <iostream>
#include <vector>
#include <set>

namespace BTree {
namespace Util {

class StringUtil {
public:
	StringUtil();
	virtual ~StringUtil();
	static std::vector<std::string> Split(std::string str, char delimiter);
	static void Split(std::string str, char delimiter, std::vector<std::string>& result);
};

} /* namespace Util */
} /* namespace BTree */

#endif /* BTREE_UTIL_STRINGUTIL_H_ */
