/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "BTree/Util/DTranxHelper.h"
#include "gtest/gtest.h"
#include "Util/ConfigHelper.h"
#include "Util/StringUtil.h"

using namespace BTree;
TEST(DTranxHelper, ReadMeta) {
	Util::ConfigHelper configHelper;
	configHelper.readFile("BTree.conf");
	std::string ipsStr = configHelper.read("IPS");
	std::vector<std::string> ips = Util::StringUtil::Split(ipsStr, ';');
	Util::DTranxHelper *dtranxHelper = new Util::DTranxHelper(60000, ips, "127.0.0.1", 30030);
	uint64_t curClientID = dtranxHelper->ReadMeta();
	std::cout << "current client id: " << curClientID << std::endl;
	uint64_t nextClientID = dtranxHelper->ReadMeta();
	EXPECT_EQ(curClientID + 1, nextClientID);
}

