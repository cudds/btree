#!/usr/bin/env python
'''
	compile proto
'''

import shutil
import subprocess

def runBash(command):
	out, err = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).communicate()
	return out
	
btreeBash = "\
	protoc -I. --cpp_out=./include/BTree Core/BTreeNode.proto;\
	rm ./include/BTree/Core/BTreeNode.pb.cc;"
runBash(btreeBash)