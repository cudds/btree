/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "BTree/Core/btree_node.h"
#include "BTree/Core/btree_mempool.h"
#include "BTree/Core/btree_helper.h"
#include "Util/ConfigHelper.h"
#include "Util/StringUtil.h"

using namespace BTree;
TEST(btree_node, serialize) {
	Util::ConfigHelper configHelper;
	configHelper.readFile("BTree.conf");
	std::string ipsStr = configHelper.read("IPS");
	std::vector<std::string> ips = Util::StringUtil::Split(ipsStr, ';');
	Util::DTranxHelper *dtranxHelper = new Util::DTranxHelper(60000, ips, "127.0.0.1", 30030);
	Util::DebugLog *debugLog = new Util::DebugLog();
	typedef Core::btree_mempool<
			Core::btree_set_params<uint64_t, std::less<uint64_t>, std::allocator<uint64_t>, 1024>> mempool_type;
	mempool_type mempool(mempool_type::allocator_type(), debugLog, true);
	mempool.InitDTranxForBTree(dtranxHelper);
	mempool_type::node_type *node = mempool.new_internal_root_node();
	node->fields_.leaf = true;
	node->fields_.values[0] = 5;
	node->fields_.values[1] = 6;
	std::string serializedStr = node->serialize();
	node->destroy();
	mempool.delete_internal_root_node(node);
	node = mempool.new_internal_root_node();
	EXPECT_EQ(0, node->fields_.values[0]);
	node->deserialize(serializedStr, "1");
	EXPECT_EQ(5, node->fields_.values[0]);
	EXPECT_EQ("1", node->selfkey());
}

