/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "BTree/Core/btree_node.h"
#include "BTree/Core/btree_mempool.h"
#include "gtest/gtest.h"
#include "Util/ConfigHelper.h"
#include "Util/StringUtil.h"

using namespace BTree;

class btree_mempool_basic: public ::testing::Test {
public:
	btree_mempool_basic() {
		Util::ConfigHelper configHelper;
		configHelper.readFile("BTree.conf");
		std::string ipsStr = configHelper.read("IPS");
		std::vector<std::string> ips = Util::StringUtil::Split(ipsStr, ';');
		Util::DTranxHelper *dtranxHelper = new Util::DTranxHelper(60000, ips, "127.0.0.1", 30030);
		Util::DebugLog *debugLog = new Util::DebugLog();
		mempool = new mempool_type(mempool_type::allocator_type(), debugLog, true);
		mempool->InitDTranxForBTree(dtranxHelper);
	}
	~btree_mempool_basic() {
	}

	typedef Core::btree_mempool<
			Core::btree_set_params<uint64_t, std::less<uint64_t>, std::allocator<uint64_t>, 1024>> mempool_type;
	mempool_type *mempool;

	btree_mempool_basic(const btree_mempool_basic&) = delete;
	btree_mempool_basic& operator=(const btree_mempool_basic&) = delete;
};

TEST_F(btree_mempool_basic, GetRoot) {
	mempool->GetRoot();
}

TEST_F(btree_mempool_basic, AddWrite) {
	mempool_type::node_type *root = mempool->new_internal_root_node();
	std::string parentKey = "parent";
	root->fields_.selfKey = "1";
	root->fields_.leaf = false;
	root->fields_.parentKey = parentKey;
	mempool->AddWrite(root->fields_.selfKey, root);
	EXPECT_TRUE(mempool->Commit());

	root = mempool->GetRoot();
	EXPECT_TRUE(root != NULL);
	EXPECT_EQ(parentKey, root->fields_.parentKey);
}

TEST_F(btree_mempool_basic, GetNode) {
	mempool_type::node_type *leaf = mempool->new_leaf_node();
	std::string parentKey = "1";
	leaf->fields_.selfKey = mempool->keySpace_->GetNextNewKey();
	leaf->fields_.leaf = true;
	leaf->fields_.parentKey = parentKey;
	mempool->AddWrite(leaf->fields_.selfKey, leaf);
	std::string tmpKey = leaf->fields_.selfKey;
	EXPECT_TRUE(mempool->Commit());
	bool alreadyRead;
	leaf = mempool->GetNode(tmpKey, alreadyRead);
	EXPECT_TRUE(leaf != NULL);
	EXPECT_EQ(true, leaf->fields_.leaf);
}
