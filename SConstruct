import os
opts = Variables()
opts.AddVariables(
    ("CC", "C Compiler"),
    ("CPPPATH", "The list of directories that the C preprocessor "
                "will search for include directories", []),
    ("CXX", "C++ Compiler"),
    ("CXXFLAGS", "Options that are passed to the C++ compiler", [])
)

env = Environment(options = opts,
				tools = ['default', 'protoc'],
				ENV = os.environ,
				CXX = 'g++-4.9')
				
env.Prepend(CXXFLAGS = [
    "-std=c++11",
    "-g"
    ])
    
env.Append(CPPPATH = ['#',  '#/include', '/usr/local/include/'])

# Define protocol buffers builder to simplify SConstruct files
def Protobuf(env, source):
    # First build the proto file
    cc = env.Protoc(os.path.splitext(source)[0] + '.pb.cc',
                    source,
                    PROTOCPROTOPATH = ["."],
                    PROTOCPYTHONOUTDIR = None,
                    PROTOCOUTDIR = ".")[1]
    # Then build the resulting C++ file with no warnings
    return env.SharedObject(cc,
                            CXXFLAGS = "-std=c++11")
env.AddMethod(Protobuf)

object_files = {}
Export('object_files')
Export('env')

SConscript('Core/SConscript', variant_dir='Build/Core')
SConscript('Util/SConscript', variant_dir='Build/Util')
SConscript('Test/SConscript', variant_dir='Build/Test')

staticLib = env.StaticLibrary("Build/btree",
                  (object_files['Core'] +
                   object_files['Util']))
                   
dynamicLib = env.SharedLibrary("Build/btree",
                  (object_files['Core'] +
                   object_files['Util']))

LibsForDTranx = ["logcabin", "protobuf", "zmq", "pthread", "cryptopp", "boost_thread", "boost_system", "boost_chrono", "pmemlog", "leveldb", "metis"]
env.Program("Build/main",(["main.cc"] + 
		object_files['Core'] +
		object_files['Util']),
		LIBS = ["dtranx"] + LibsForDTranx)
		
env.Alias('install-library', env.Install('/usr/local/lib', [staticLib, dynamicLib]))
env.Alias('install-header', env.Install('/usr/local/include', ['include/BTree']))
env.Alias('install', ['install-library', 'install-header'])