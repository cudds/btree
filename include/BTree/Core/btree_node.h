/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef BTREE_CORE_BTREE_NODE_H_
#define BTREE_CORE_BTREE_NODE_H_

#include <unordered_set>
#include "BTree/Core/BTreeNode.pb.h"
#include "BTree/Core/btree_helper.h"
#include "BTree/Util/DTranxHelper.h"
#include "BTree/Util/LastNotCommitException.h"
#include "BTree/Util/DebugLog.h"
#include "BTree/Util/DTranxKeySpace.h"

namespace BTree {
namespace Core {

template<typename Params>
class btree_mempool;
// A node in the btree holding. The same node type is used for both internal
// and leaf nodes in the btree, though the nodes are allocated in such a way
// that the children array is only valid in internal nodes.
template<typename Params>
class btree_node {
public:
	typedef Params params_type;
	typedef btree_node<Params> self_type;
	typedef typename Params::key_type key_type;
	typedef typename Params::data_type data_type;
	typedef typename Params::value_type value_type;
	typedef typename Params::mutable_value_type mutable_value_type;
	typedef typename Params::pointer pointer;
	typedef typename Params::const_pointer const_pointer;
	typedef typename Params::reference reference;
	typedef typename Params::const_reference const_reference;
	typedef typename Params::key_compare key_compare;
	typedef typename Params::size_type size_type;
	typedef typename Params::difference_type difference_type;
	typedef btree_mempool<Params> mempool_type;
	// Typedefs for the various types of node searches.
	typedef btree_linear_search_plain_compare<key_type, self_type, key_compare> linear_search_plain_compare_type;
	typedef btree_linear_search_compare_to<key_type, self_type, key_compare> linear_search_compare_to_type;
	typedef btree_binary_search_plain_compare<key_type, self_type, key_compare> binary_search_plain_compare_type;
	typedef btree_binary_search_compare_to<key_type, self_type, key_compare> binary_search_compare_to_type;
	// If we have a valid key-compare-to type, use linear_search_compare_to,
	// otherwise use linear_search_plain_compare.
	typedef typename if_<Params::is_key_compare_to::value, linear_search_compare_to_type,
			linear_search_plain_compare_type>::type linear_search_type;
	// If we have a valid key-compare-to type, use binary_search_compare_to,
	// otherwise use binary_search_plain_compare.
	typedef typename if_<Params::is_key_compare_to::value, binary_search_compare_to_type,
			binary_search_plain_compare_type>::type binary_search_type;
	// If the key is an integral or floating point type, use linear search which
	// is faster than binary search for such types. Might be wise to also
	// configure linear search based on node-size.
	typedef typename if_<
			std::is_integral<key_type>::value || std::is_floating_point<key_type>::value,
			linear_search_type, binary_search_type>::type search_type;

	struct base_fields {
		typedef typename Params::node_count_type field_type;

		// A boolean indicating whether the node is a leaf or not.
		bool leaf;
		// The position of the node in the node's parent.
		field_type position;
		// The maximum number of values the node can hold.
		field_type max_count;
		// The count of the number of values in the node.
		field_type count;
		// A pointer to the node's parent.
		btree_node *parent;
		std::string parentKey;
		std::string selfKey;
	};

	enum {
		kValueSize = params_type::kValueSize, kTargetNodeSize = params_type::kTargetNodeSize,

		// Compute how many values we can fit onto a leaf node.
		kNodeTargetValues = (kTargetNodeSize - sizeof(base_fields)) / kValueSize,
		// We need a minimum of 3 values per internal node in order to perform
		// splitting (1 value for the two nodes involved in the split and 1 value
		// propagated to the parent as the delimiter for the split).
		kNodeValues = kNodeTargetValues >= 3 ? kNodeTargetValues : 3,

		kExactMatch = 1 << 30,
		kMatchMask = kExactMatch - 1,
	};

	struct leaf_fields: public base_fields {
		// The array of values. Only the first count of these values have been
		// constructed and are valid.
		mutable_value_type values[kNodeValues];
	};

	struct internal_fields: public leaf_fields {
		// The array of child pointers. The keys in children_[i] are all less than
		// key(i). The keys in children_[i + 1] are all greater than key(i). There
		// are always count + 1 children.
		btree_node *children[kNodeValues + 1];
		/*
		 * some of the children might be NULL, so use Util::DtranxHelper::NOSUCHNODE
		 */
		std::string childrenKey[kNodeValues + 1];
	};

	struct root_fields: public internal_fields {
		btree_node *rightmost;
		std::string rightmostKey;
	};

public:
	btree_node() {
		fields_.rightmost = NULL;
		fields_.rightmostKey = Util::DTranxKeySpace::NOSUCHNODE;
		for (int i = 0; i < kNodeValues + 1; ++i) {
			fields_.childrenKey[i] = Util::DTranxKeySpace::NOSUCHNODE;
			fields_.children[i] = NULL;
		}
		for (int i = 0; i < kNodeValues; ++i) {
			fields_.values[i] = 0;
		}

		fields_.leaf = false;
		fields_.position = 0;
		fields_.count = 0;
		fields_.max_count = 0;
		fields_.parent = NULL;
		fields_.parentKey = Util::DTranxKeySpace::NOSUCHNODE;
		fields_.selfKey = Util::DTranxKeySpace::NOSUCHNODE;

	}
	// Getter/setter for whether this is a leaf node or not. This value doesn't
	// change after the node is created.
	bool leaf() const {
		return fields_.leaf;
	}

	// Getter for the position of this node in its parent.
	int position() const {
		return fields_.position;
	}
	void set_position(int v) {
		fields_.position = v;
	}

	// Getter/setter for the number of values stored in this node.
	int count() const {
		return fields_.count;
	}
	void set_count(int v) {
		fields_.count = v;
	}
	int max_count() const {
		return fields_.max_count;
	}

	// Getter for the parent of this node.

	btree_node* parent(mempool_type *mempool, bool checkValid = true) {
		assert(fields_.parentKey != Util::DTranxKeySpace::NOSUCHNODE);
		if (fields_.parent == NULL && fields_.parentKey != Util::DTranxKeySpace::NOSUCHNODE) {
			/*
			 * retrieve nodes from database
			 */
			bool alreadyRead;
			fields_.parent = mempool->GetNode(fields_.parentKey, alreadyRead);
			if (fields_.parent == NULL) {
				throw Util::LastNotCommitException("lasttranxnotcommit in node::parent1\n");
			}
			if (!is_root() && checkValid) {
				if (fields_.parent->selfkey() != fields_.parentKey) {
					throw Util::LastNotCommitException("lasttranxnotcommit in node::parent2\n");
				}
				if (fields_.parent->fields_.childrenKey[fields_.position] != fields_.selfKey) {
					throw Util::LastNotCommitException("lasttranxnotcommit in node::parent3\n");
				}
				if (fields_.parent->fields_.count == 0) {
					throw Util::LastNotCommitException("lasttranxnotcommit in node::parent4\n");
				}
			}
		}
		return fields_.parent;
	}

	btree_node** mutable_parent() {
		return &fields_.parent;
	}
	/*
	 * Getter for the self key
	 */
	std::string selfkey() const {
		return fields_.selfKey;
	}

	void set_selfkey(std::string selfkey) {
		fields_.selfKey = selfkey;
	}

	void set_rightmostkey(std::string rightmostkey, mempool_type *mempool) {
		fields_.rightmostKey = rightmostkey;
	}
	// Getter for whether the node is the root of the tree. The parent of the
	// root of the tree is the leftmost node in the tree which is guaranteed to
	// be a leaf.
	bool is_root() const {
		/*
		 * use selfKey instead of parent pointer
		 */
		return fields_.selfKey == Util::DTranxKeySpace::ROOTNODEKEY;
	}
	/*
	 * make_root is called when the root has no values and only this one child
	 * and this node is the leaf, simply make it the root
	 */
	void make_root(mempool_type *mempool) {
		std::string tmpSelfKey = fields_.selfKey;
		self_type *tmpParent = parent(mempool);
		fields_.selfKey = Util::DTranxKeySpace::ROOTNODEKEY;
		fields_.parent = parent(mempool)->parent(mempool);
		fields_.parentKey = parent(mempool)->selfkey();
		tmpParent->set_selfkey(tmpSelfKey);
	}

	// Getter for the rightmost root node field. Only valid on the root node.
	btree_node* rightmost(mempool_type *mempool) {
		if (fields_.rightmost == NULL && fields_.rightmostKey != Util::DTranxKeySpace::NOSUCHNODE) {
			bool alreadyRead;
			fields_.rightmost = mempool->GetNode(fields_.rightmostKey, alreadyRead);
			if (fields_.rightmost == NULL) {
				throw Util::LastNotCommitException("lasttranxnotcommit in node::rightmost1\n");
			}
			if (!alreadyRead && fields_.rightmost->fields_.count == 0) {
				throw Util::LastNotCommitException(
						"lasttranxnotcommit in node::rightmost2, key is " + fields_.rightmostKey
								+ " and selfkey is " + selfkey() + "\n");
			}
		}
		return fields_.rightmost;
	}

	btree_node** mutable_rightmost() {
		return &fields_.rightmost;
	}

	// Getters for the key/value at position i in the node.
	const key_type& key(int i) const {
		return params_type::key(fields_.values[i]);
	}
	reference value(int i) {
		return reinterpret_cast<reference>(fields_.values[i]);
	}
	const_reference value(int i) const {
		return reinterpret_cast<const_reference>(fields_.values[i]);
	}

	mutable_value_type* mutable_value(int i) {
		return &fields_.values[i];
	}

	// Swap value i in this node with value j in node x.
	void value_swap(int i, btree_node *x, int j, mempool_type *mempool) {
		params_type::swap(mutable_value(i), x->mutable_value(j));
	}

	// Getters/setter for the child at position i in the node.
	btree_node* child(int i, mempool_type *mempool, bool checkValid = true) {
		if (fields_.children[i] == NULL
				&& fields_.childrenKey[i] != Util::DTranxKeySpace::NOSUCHNODE) {
			bool alreadyRead;
			fields_.children[i] = mempool->GetNode(fields_.childrenKey[i], alreadyRead);
			if (fields_.children[i] == NULL) {
				throw Util::LastNotCommitException("lasttranxnotcommit in node::child1\n");
			}
			if (checkValid) {
				if (fields_.children[i]->fields_.parentKey != fields_.selfKey) {
					throw Util::LastNotCommitException(
							"lasttranxnotcommit in node::child2: " + fields_.selfKey + "\n");
				}
				if (i != fields_.children[i]->fields_.position) {
					throw Util::LastNotCommitException("lasttranxnotcommit in node::child3\n");
				}
				if (fields_.children[i]->fields_.count == 0) {
					throw Util::LastNotCommitException("lasttranxnotcommit in node::child4\n");
				}
			}
		}
		if (fields_.children[i] == NULL) {
			throw Util::LastNotCommitException("lasttranxnotcommit in node::child4\n");
		}
		return fields_.children[i];
	}

	btree_node** mutable_child(int i) {
		return &fields_.children[i];
	}

	void set_child(int i, btree_node *c, mempool_type *mempool) {
		*mutable_child(i) = c;
		fields_.childrenKey[i] = c->selfkey();
		c->fields_.parent = this;
		c->fields_.parentKey = fields_.selfKey;
		c->fields_.position = i;
	}

	// Returns the position of the first value whose key is not less than k.
	template<typename Compare>
	int lower_bound(const key_type &k, const Compare &comp) const {
		return search_type::lower_bound(k, BTREE_CORE_BTREE_NODE_H_ *this, comp);
	}
	// Returns the position of the first value whose key is greater than k.
	template<typename Compare>
	int upper_bound(const key_type &k, const Compare &comp) const {
		return search_type::upper_bound(k, *this, comp);
	}

	// Returns the position of the first value whose key is not less than k using
	// linear search performed using plain compare.
	template<typename Compare>
	int linear_search_plain_compare(const key_type &k, int s, int e, const Compare &comp) const {
		while (s < e) {
			if (!btree_compare_keys(comp, key(s), k)) {
				break;
			}
			++s;
		}
		return s;
	}

	// Returns the position of the first value whose key is not less than k using
	// linear search performed using compare-to.
	template<typename Compare>
	int linear_search_compare_to(const key_type &k, int s, int e, const Compare &comp) const {
		while (s < e) {
			int c = comp(key(s), k);
			if (c == 0) {
				return s | kExactMatch;
			} else if (c > 0) {
				break;
			}
			++s;
		}
		return s;
	}

	// Returns the position of the first value whose key is not less than k using
	// binary search performed using plain compare.
	template<typename Compare>
	int binary_search_plain_compare(const key_type &k, int s, int e, const Compare &comp) const {
		while (s != e) {
			int mid = (s + e) / 2;
			if (btree_compare_keys(comp, key(mid), k)) {
				s = mid + 1;
			} else {
				e = mid;
			}
		}
		return s;
	}

	// Returns the position of the first value whose key is not less than k using
	// binary search performed using compare-to.
	template<typename CompareTo>
	int binary_search_compare_to(const key_type &k, int s, int e, const CompareTo &comp) const {
		while (s != e) {
			int mid = (s + e) / 2;
			int c = comp(key(mid), k);
			if (c < 0) {
				s = mid + 1;
			} else if (c > 0) {
				e = mid;
			} else {
				// Need to return the first value whose key is not less than k, which
				// requires continuing the binary search. Note that we are guaranteed
				// that the result is an exact match because if "key(mid-1) < k" the
				// call to binary_search_compare_to() will return "mid".
				s = binary_search_compare_to(k, s, mid, comp);
				return s | kExactMatch;
			}
		}
		return s;
	}

	// Inserts the value x at position i, shifting all existing values and
	// children at positions >= i to the right by 1.
	void insert_value(int i, const value_type &x, mempool_type *mempool);

	// Removes the value at position i, shifting all existing values and children
	// at positions > i to the left by 1.
	void remove_value(int i, mempool_type *mempool);

	// Rebalances a node with its right sibling.
	void rebalance_right_to_left(btree_node *sibling, int to_move, mempool_type *mempool);
	void rebalance_left_to_right(btree_node *sibling, int to_move, mempool_type *mempool);

	// Splits a node, moving a portion of the node's values to its right sibling.
	void split(btree_node *sibling, int insert_position, mempool_type *mempool);

	// Merges a node with its right sibling, moving all of the values and the
	// delimiting key in the parent node onto itself.
	void merge(btree_node *sibling, mempool_type *mempool);

	// Swap the contents of "this" and "src".
	void swap(btree_node *src, mempool_type *mempool);

	/*
	 * Node allocation/deletion routines.set_child
	 */
	template<typename P>
	static btree_node<P> *init_leaf(btree_node<P> *f, btree_node<P> *parent,
			mempool_type *mempool) {
		assert(parent != NULL);
		assert(f != NULL);
		f->fields_.leaf = 1;
		f->fields_.position = 0;
		f->fields_.max_count = kNodeValues;
		f->fields_.count = 0;
		f->fields_.parent = parent;
		f->fields_.parentKey = parent->fields_.selfKey;
		for (int i = 0; i < kNodeValues; ++i) {
			f->fields_.values[i] = 0;
		}
		return f;
	}

	template<typename P>
	static btree_node<P> *init_internal(btree_node<P> *f, btree_node<P> *parent,
			mempool_type *mempool) {
		init_leaf(f, parent, mempool);
		f->fields_.leaf = 0;
		if (!NDEBUG) {
			memset(f->fields_.children, 0, sizeof(f->fields_.children));
		}
		return f;
	}
	template<typename P>
	static btree_node<P> *init_root(btree_node<P> *f, btree_node<P> *parent,
			mempool_type *mempool) {
		f->fields_.selfKey = Util::DTranxKeySpace::ROOTNODEKEY;
		init_internal(f, parent, mempool);
		f->fields_.rightmost = parent;
		f->fields_.rightmostKey = parent->selfkey();
		return f;
	}

	void destroy() {
		for (int i = 0; i < count(); ++i) {
			value_destroy(i);
		}
	}

	/*
	 * serialize/de-serialize functions
	 */
	std::string serialize() {
		//selfKey is not stored in serialized string since it can be assigned during retrieving
		RootFields rootFields;
		rootFields.set_rightmost(fields_.rightmostKey);

		for (int i = 0; i < kNodeValues + 1; ++i) {
			rootFields.mutable_internalfields()->add_children(fields_.childrenKey[i]);
		}

		for (int i = 0; i < kNodeValues; ++i) {
			rootFields.mutable_internalfields()->mutable_leaffields()->add_values(
					fields_.values[i]);
		}
		rootFields.mutable_internalfields()->mutable_leaffields()->mutable_basefields()->set_leaf(
				fields_.leaf);
		rootFields.mutable_internalfields()->mutable_leaffields()->mutable_basefields()->set_position(
				fields_.position);
		rootFields.mutable_internalfields()->mutable_leaffields()->mutable_basefields()->set_max_count(
				fields_.max_count);
		rootFields.mutable_internalfields()->mutable_leaffields()->mutable_basefields()->set_count(
				fields_.count);
		rootFields.mutable_internalfields()->mutable_leaffields()->mutable_basefields()->set_parent(
				fields_.parentKey);
		return rootFields.SerializeAsString();
	}

	void deserialize(std::string content, std::string selfKey) {
		RootFields rootFields;
		rootFields.Clear();
		rootFields.ParseFromString(content);
		if (rootFields.has_rightmost()) {
			fields_.rightmostKey = rootFields.rightmost();
		}
		int index = 0;
		for (auto it = rootFields.internalfields().children().begin();
				it != rootFields.internalfields().children().end(); ++it) {
			fields_.childrenKey[index++] = *it;
		}

		index = 0;
		for (auto it = rootFields.internalfields().leaffields().values().begin();
				it != rootFields.internalfields().leaffields().values().end(); ++it) {
			fields_.values[index++] = *it;
		}
		fields_.leaf = rootFields.internalfields().leaffields().basefields().leaf();
		fields_.position = rootFields.internalfields().leaffields().basefields().position();
		fields_.max_count = rootFields.internalfields().leaffields().basefields().max_count();
		fields_.count = rootFields.internalfields().leaffields().basefields().count();
		fields_.parentKey = rootFields.internalfields().leaffields().basefields().parent();
		fields_.selfKey = selfKey;
	}

	/*
	 * If anything changes with the Print function,
	 * please adjust the Scripts/ProcessDebug.py
	 */
	std::string Print(bool isIterative = false) {
		std::stringstream ss;
		ss << Util::NODE_PRINT_START;
		ss << "maximum number of values: " << kNodeValues << "\n";
		ss << "selfkey: " << selfkey() << "\n";
		ss << "leaf: " << fields_.leaf << "\n";
		ss << "position: " << int(fields_.position) << "\n";
		ss << "max_count: " << int(fields_.max_count) << "\n";
		ss << "count: " << int(fields_.count) << "\n";
		ss << "parentKey: " << fields_.parentKey << "\n";
		ss << "values: " << "\n";
		for (int i = 0; i < kNodeValues; ++i) {
			ss << fields_.values[i] << " ";
		}
		ss << "\n";
		ss << "children: " << "\n";
		for (int i = 0; i < kNodeValues + 1; ++i) {
			ss << fields_.childrenKey[i] << " ";
		}
		ss << "\n";
		ss << "rightmostKey: " << fields_.rightmostKey << "\n";
		ss << Util::NODE_PRINT_END;

		if (!isIterative) {
			return ss.str();
		}

		for (int i = 0; i < kNodeValues + 1; ++i) {
			if (fields_.children[i] != NULL) {
				ss << fields_.children[i]->Print();
			}
		}
		return ss.str();
	}

	/*
	 * CopyNode copies the fields that'll be serialized later
	 * IsNodeDifferent only compares the fields that'll be serialized later
	 * These two functions are used for automatic writeset detection
	 *
	 * ResetPointerKey resets pointer key fields to NULL
	 * in order to detect readset.
	 */
	void CopyNode(btree_node* other);

	bool IsNodeDifferent(btree_node* other);

	void ResetPointerKey();

private:
	void value_init(int i) {
		new (&fields_.values[i]) mutable_value_type;
	}
	void value_init(int i, const value_type &x) {
		new (&fields_.values[i]) mutable_value_type(x);
	}
	void value_destroy(int i) {
		fields_.values[i] = 0;
		//fields_.values[i].~mutable_value_type();
	}

private:
	root_fields fields_;

private:
	btree_node(const btree_node&);
	void operator=(const btree_node&);
};

///////////////////////////////////////////////////////////////////////////////////
/*
 * Implementation
 */
template<typename P>
inline void btree_node<P>::insert_value(int i, const value_type &x, mempool_type *mempool) {
	value_init(count(), x);
	for (int j = count(); j > i; --j) {
		value_swap(j, this, j - 1, mempool);
	}
	set_count(count() + 1);

	if (!leaf()) {
		++i;
		for (int j = count(); j > i; --j) {
			*mutable_child(j) = child(j - 1, mempool);
			btree_node *tmpNode = child(j, mempool);
			tmpNode->set_position(j);
			fields_.childrenKey[j] = child(j, mempool)->selfkey();
		}
		*mutable_child(i) = NULL;
		fields_.childrenKey[i] = Util::DTranxKeySpace::NOSUCHNODE;
	}
}

template<typename P>
inline void btree_node<P>::remove_value(int i, mempool_type *mempool) {
	if (!leaf()) {
		for (int j = i + 1; j < count(); ++j) {
			*mutable_child(j) = child(j + 1, mempool);
			child(j, mempool)->set_position(j);
			fields_.childrenKey[j] = fields_.childrenKey[j + 1];
		}
		*mutable_child(count()) = NULL;
		fields_.childrenKey[count()] = Util::DTranxKeySpace::NOSUCHNODE;
	}

	set_count(count() - 1);
	for (; i < count(); ++i) {
		value_swap(i, this, i + 1, mempool);
	}
	value_destroy(i);
}

template<typename P>
void btree_node<P>::rebalance_right_to_left(btree_node *src, int to_move, mempool_type *mempool) {
	assert(src != NULL);
	assert(parent(mempool) == src->parent(mempool));
	assert(position() + 1 == src->position());
	assert(src->count() >= count());
	assert(to_move >= 1);
	assert(to_move <= src->count());

	// Make room in the left node for the new values.
	for (int i = 0; i < to_move; ++i) {
		value_init(i + count());
	}

	// Move the delimiting value to the left node and the new delimiting value
	// from the right node.
	value_swap(count(), parent(mempool), position(), mempool);
	parent(mempool)->value_swap(position(), src, to_move - 1, mempool);

	// Move the values from the right to the left node.
	for (int i = 1; i < to_move; ++i) {
		value_swap(count() + i, src, i - 1, mempool);
	}
	// Shift the values in the right node to their correct position.
	for (int i = to_move; i < src->count(); ++i) {
		src->value_swap(i - to_move, src, i, mempool);
	}
	for (int i = 1; i <= to_move; ++i) {
		src->value_destroy(src->count() - i);
	}

	if (!leaf()) {
		// Move the child pointers from the right to the left node.
		for (int i = 0; i < to_move; ++i) {
			set_child(1 + count() + i, src->child(i, mempool), mempool);
		}
		for (int i = 0; i <= src->count() - to_move; ++i) {
			src->set_child(i, src->child(i + to_move, mempool), mempool);
			*src->mutable_child(i + to_move) = NULL;
			src->fields_.childrenKey[i + to_move] = Util::DTranxKeySpace::NOSUCHNODE;
		}
	}

	// Fixup the counts on the src and dest nodes.
	set_count(count() + to_move);
	src->set_count(src->count() - to_move);
}

template<typename P>
void btree_node<P>::rebalance_left_to_right(btree_node *dest, int to_move, mempool_type *mempool) {
	assert(dest != NULL);
	assert(parent(mempool) == dest->parent(mempool));
	assert(position() + 1 == dest->position());
	assert(dest->count() <= count());
	assert(to_move >= 1);
	assert(to_move <= count());

	// Make room in the right node for the new values.
	for (int i = 0; i < to_move; ++i) {
		dest->value_init(i + dest->count());
	}
	for (int i = dest->count() - 1; i >= 0; --i) {
		dest->value_swap(i, dest, i + to_move, mempool);
	}

	// Move the delimiting value to the right node and the new delimiting value
	// from the left node.
	dest->value_swap(to_move - 1, parent(mempool), position(), mempool);
	parent(mempool)->value_swap(position(), this, count() - to_move, mempool);
	value_destroy(count() - to_move);

	// Move the values from the left to the right node.
	for (int i = 1; i < to_move; ++i) {
		value_swap(count() - to_move + i, dest, i - 1, mempool);
		value_destroy(count() - to_move + i);
	}

	if (!leaf()) {
		// Move the child pointers from the left to the right node.
		for (int i = dest->count(); i >= 0; --i) {
			dest->set_child(i + to_move, dest->child(i, mempool), mempool);
			*dest->mutable_child(i) = NULL;
			dest->fields_.childrenKey[i] = Util::DTranxKeySpace::NOSUCHNODE;
		}
		for (int i = 1; i <= to_move; ++i) {
			dest->set_child(i - 1, child(count() - to_move + i, mempool), mempool);
			*mutable_child(count() - to_move + i) = NULL;
			fields_.childrenKey[count() - to_move + i] = Util::DTranxKeySpace::NOSUCHNODE;
		}
	}

	// Fixup the counts on the src and dest nodes.
	set_count(count() - to_move);
	dest->set_count(dest->count() + to_move);
}

template<typename P>
void btree_node<P>::split(btree_node *dest, int insert_position, mempool_type *mempool) {
	// We bias the split based on the position being inserted. If we're
	// inserting at the beginning of the left node then bias the split to put
	// more values on the right node. If we're inserting at the end of the
	// right node then bias the split to put more values on the left node.
	if (insert_position == 0) {
		dest->set_count(count() - 1);
	} else if (insert_position == max_count()) {
		dest->set_count(0);
	} else {
		dest->set_count(count() / 2);
	}
	set_count(count() - dest->count());
	// Move values from the left sibling to the right sibling.
	for (int i = 0; i < dest->count(); ++i) {
		dest->value_init(i);
		value_swap(count() + i, dest, i, mempool);
		value_destroy(count() + i);
	}
	// The split key is the largest value in the left sibling.
	set_count(count() - 1);
	parent(mempool)->insert_value(position(), value_type(), mempool);
	value_swap(count(), parent(mempool), position(), mempool);
	value_destroy(count());
	parent(mempool)->set_child(position() + 1, dest, mempool);
	if (!leaf()) {
		/*
		 * when an internal node is split, it moves some children to its sibling
		 */
		for (int i = 0; i <= dest->count(); ++i) {
			btree_node *tmpNode = child(count() + i + 1, mempool);
			dest->set_child(i, tmpNode, mempool);
			*mutable_child(count() + i + 1) = NULL;
			fields_.childrenKey[count() + i + 1] = Util::DTranxKeySpace::NOSUCHNODE;
		}
	}
}

template<typename P>
void btree_node<P>::merge(btree_node *src, mempool_type *mempool) {
	// Move the delimiting value to the left node.
	value_init(count());
	value_swap(count(), parent(mempool), position(), mempool);

	// Move the values from the right to the left node.
	for (int i = 0; i < src->count(); ++i) {
		value_init(1 + count() + i);
		value_swap(1 + count() + i, src, i, mempool);
		src->value_destroy(i);
	}

	if (!leaf()) {
		// Move the child pointers from the right to the left node.
		for (int i = 0; i <= src->count(); ++i) {
			btree_node *tmpNode = src->child(i, mempool);
			set_child(1 + count() + i, tmpNode, mempool);
			*src->mutable_child(i) = NULL;
			src->fields_.childrenKey[i] = Util::DTranxKeySpace::NOSUCHNODE;
		}
	}

	// Fixup the counts on the src and dest nodes.
	set_count(1 + count() + src->count());
	src->set_count(0);

	// Remove the value on the parent node.
	parent(mempool)->remove_value(position(), mempool);

}

template<typename P>
void btree_node<P>::swap(btree_node *x, mempool_type *mempool) {
	assert(leaf() == x->leaf());

	// Swap the values.
	for (int i = count(); i < x->count(); ++i) {
		value_init(i);
	}
	for (int i = x->count(); i < count(); ++i) {
		x->value_init(i);
	}
	int n = std::max(count(), x->count());
	for (int i = 0; i < n; ++i) {
		value_swap(i, x, i, mempool);
	}
	for (int i = count(); i < x->count(); ++i) {
		x->value_destroy(i);
	}
	for (int i = x->count(); i < count(); ++i) {
		value_destroy(i);
	}

	if (!leaf()) {
		// Swap the child pointers.
		for (int i = 0; i <= n; ++i) {
			btree_swap_helper(*mutable_child(i), *x->mutable_child(i));
			std::string childStr = x->fields_.childrenKey[i];
			x->fields_.childrenKey[i] = fields_.childrenKey[i];
			fields_.childrenKey[i] = childStr;
		}
		for (int i = 0; i <= count(); ++i) {
			x->child(i, mempool, false)->fields_.parent = x;
			x->child(i, mempool, false)->fields_.parentKey = x->selfkey();
		}
		for (int i = 0; i <= x->count(); ++i) {
			child(i, mempool, false)->fields_.parent = this;
			child(i, mempool, false)->fields_.parentKey = this->selfkey();
		}
	}

	// Swap the counts.
	btree_swap_helper(fields_.count, x->fields_.count);
}

template<typename P>
void btree_node<P>::CopyNode(btree_node* other) {
	other->fields_.rightmostKey = fields_.rightmostKey;

	for (int i = 0; i < kNodeValues + 1; ++i) {
		other->fields_.childrenKey[i] = fields_.childrenKey[i];
	}

	for (int i = 0; i < kNodeValues; ++i) {
		other->fields_.values[i] = fields_.values[i];
	}

	other->fields_.leaf = fields_.leaf;
	other->fields_.position = fields_.position;
	other->fields_.max_count = fields_.max_count;
	other->fields_.count = fields_.count;
	other->fields_.parentKey = fields_.parentKey;
	other->fields_.selfKey = fields_.selfKey;
}

template<typename P>
bool btree_node<P>::IsNodeDifferent(btree_node* other) {
	if (other->fields_.rightmostKey != fields_.rightmostKey) {
		return true;
	}
	if (other->fields_.count != fields_.count) {
		return true;
	}
	for (int i = 0; i < kNodeValues + 1; ++i) {
		if (other->fields_.childrenKey[i] != fields_.childrenKey[i]) {
			return true;
		}
	}

	for (int i = 0; i < kNodeValues; ++i) {
		if (other->fields_.values[i] != fields_.values[i]) {
			return true;
		}
	}

	if (other->fields_.leaf != fields_.leaf) {
		return true;
	}
	if (other->fields_.position != fields_.position) {
		return true;
	}
	if (other->fields_.max_count != fields_.max_count) {
		return true;
	}
	if (other->fields_.parentKey != fields_.parentKey) {
		return true;
	}
	if (other->fields_.selfKey != fields_.selfKey) {
		return true;
	}
	return false;
}

template<typename P>
void btree_node<P>::ResetPointerKey() {
	fields_.rightmost = NULL;
	for (int i = 0; i < kNodeValues + 1; ++i) {
		fields_.children[i] = NULL;
	}
	fields_.parent = NULL;
}

}  // namespace Core
}  // namespace BTree
#endif /* BTREE_CORE_BTREE_NODE_H_ */
