/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef BTREE_Core_BTREE_ITERATOR_H_
#define BTREE_Core_BTREE_ITERATOR_H_
#include "BTree/Core/btree_include.h"
#include "BTree/Core/btree_mempool.h"
#include <iterator>

namespace BTree {
namespace Core {
template<typename Node, typename Reference, typename Pointer>
struct btree_iterator {
	typedef typename Node::key_type key_type;
	typedef typename Node::size_type size_type;
	typedef typename Node::difference_type difference_type;
	typedef typename Node::params_type params_type;
	typedef btree_mempool<params_type> mempool_type;

	typedef Node node_type;
	typedef typename std::remove_const<Node>::type normal_node;
	typedef const Node const_node;
	typedef typename params_type::value_type value_type;
	typedef typename params_type::pointer normal_pointer;
	typedef typename params_type::reference normal_reference;
	typedef typename params_type::const_pointer const_pointer;
	typedef typename params_type::const_reference const_reference;

	typedef Pointer pointer;
	typedef Reference reference;
	typedef std::bidirectional_iterator_tag iterator_category;

	typedef btree_iterator<normal_node, normal_reference, normal_pointer> iterator;
	typedef btree_iterator<const_node, const_reference, const_pointer> const_iterator;
	typedef btree_iterator<Node, Reference, Pointer> self_type;

	btree_iterator()
			: node(NULL), position(-1), mempool_(NULL), isNULL(false)  {
	}
	btree_iterator(Node *n, int p, mempool_type *mempool)
			: node(n), position(p), mempool_(mempool), isNULL(false) {
	}
	btree_iterator(const iterator &x)
			: node(x.node), position(x.position), mempool_(x.mempool_) , isNULL(false) {
	}

	// Increment/decrement the iterator.
	void increment() {
		if (node->leaf() && ++position < node->count()) {
			return;
		}
		increment_slow();
	}
	void increment_by(int count);
	void increment_slow();

	void decrement() {
		if (node->leaf() && --position >= 0) {
			return;
		}
		decrement_slow();
	}
	void decrement_slow();

	void setNULL(){
		isNULL = true;
	}

	bool getNULL(){
		return isNULL;
	}

	bool operator==(const const_iterator &x) const {
		return node == x.node && position == x.position;
	}
	bool operator!=(const const_iterator &x) const {
		return node != x.node || position != x.position;
	}

	// Accessors for the key/value the iterator is pointing at.
	const key_type& key() const {
		return node->key(position);
	}
	reference operator*() const {
		return node->value(position);
	}
	pointer operator->() const {
		return &node->value(position);
	}

	self_type& operator++() {
		increment();
		return *this;
	}
	self_type& operator--() {
		decrement();
		return *this;
	}
	self_type operator++(int) {
		self_type tmp = *this;
		++*this;
		return tmp;
	}
	self_type operator--(int) {
		self_type tmp = *this;
		--*this;
		return tmp;
	}

	// The node in the tree the iterator is pointing at.
	Node *node;
	// The position within the node of the tree the iterator is pointing at.
	int position;
	/*
	 * isNULL is used for find_unique/insert_unique/erase_unique to detect whether the last
	 * transaction is complete
	 */
	bool isNULL;
	/*
	 * mempool pointer
	 */
	mempool_type *mempool_;
};

template<typename N, typename R, typename P>
void btree_iterator<N, R, P>::increment_slow() {
	if (node->leaf()) {
		assert(position >= node->count());
		self_type save(*this);
		while (position == node->count() && !node->is_root()) {
			position = node->position();
			node = node->parent(mempool_);
		}
		if (position == node->count()) {
			*this = save;
		}
	} else {
		assert(position < node->count());
		node = node->child(position+1, mempool_);
		while (!node->leaf()) {
			node = node->child(0, mempool_);
		}
		position = 0;
	}
}

template<typename N, typename R, typename P>
void btree_iterator<N, R, P>::increment_by(int count) {
	while (count > 0) {
		if (node->leaf()) {
			int rest = node->count() - position;
			position += std::min(rest, count);
			count = count - rest;
			if (position < node->count()) {
				return;
			}
		} else {
			--count;
		}
		increment_slow();
	}
}

template<typename N, typename R, typename P>
void btree_iterator<N, R, P>::decrement_slow() {
	if (node->leaf()) {
		assert(position <= -1);
		self_type save(*this);
		while (position < 0 && !node->is_root()) {
			//assert(node->parent()->child(node->position()) == node);
			position = node->position() - 1;
			node = node->parent(mempool_);
		}
		if (position < 0) {
			*this = save;
		}
	} else {
		assert(position >= 0);
		node = node->child(position, mempool_);

		while (!node->leaf()) {
			node = node->child(node->count(), mempool_);
		}
		position = node->count() - 1;
	}
}

}  // namespace Core
}  // namespace BTree

#endif /* BTREE_Core_BTREE_ITERATOR_H_ */
