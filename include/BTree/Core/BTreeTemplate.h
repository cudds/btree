/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef BTREE_CORE_BTREETEMPLATE_H_
#define BTREE_CORE_BTREETEMPLATE_H_

#include "BTree/Core/btree.h"
/*
 * NodeSize will be the maximum size of a value in the key-value store
 * calculate as (NodeSize - 32) / 8 for uint64_t
 */
namespace BTree {
namespace Core {
template<typename KeyType, int NodeSize = 264>
class BTreeTemplate: public Core::btree<
		Core::btree_set_params<KeyType, std::less<KeyType>, std::allocator<KeyType>, NodeSize>> {
	typedef Core::btree_set_params<KeyType, std::less<KeyType>, std::allocator<KeyType>, NodeSize> params_type;
	typedef Core::btree<params_type> btree_type;

public:
	typedef typename btree_type::key_compare key_compare;
	typedef typename btree_type::allocator_type allocator_type;
	BTreeTemplate()
			: btree_type(key_compare(), allocator_type()) {
	}
	BTreeTemplate(
	bool enableDebug, std::string debugFilename = "btree.debug", bool poolCached = true)
			: btree_type(key_compare(), allocator_type(), enableDebug, debugFilename, poolCached) {
	}

};
}  // namespace Core
}  // namespace BTree

#endif /* BTREE_CORE_BTREETEMPLATE_H_ */
