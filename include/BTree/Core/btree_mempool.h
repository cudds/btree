/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */
#ifndef BTREE_CORE_BTREE_MEMPOOL_H__
#define BTREE_CORE_BTREE_MEMPOOL_H__

#include <unordered_set>
#include "BTree/Util/DTranxHelper.h"
#include "BTree/Util/DebugLog.h"
#include "BTree/Util/DTranxKeySpace.h"

namespace BTree {
namespace Core {

template<typename Params>
class btree_mempool {
	//MaybeLater: collect deleted keys
private:
	typedef btree_mempool<Params> self_type;
	typedef btree_node<Params> node_type;
	typedef typename node_type::base_fields base_fields;
	typedef typename node_type::leaf_fields leaf_fields;
	typedef typename node_type::root_fields root_fields;
	typedef typename node_type::internal_fields internal_fields;

public:
	typedef typename Params::allocator_type allocator_type;
	typedef typename allocator_type::template rebind<char>::other internal_allocator_type;
	typedef typename Params::value_type value_type;

	enum {
		kNodeValues = node_type::kNodeValues
	};

public:
	btree_mempool(const allocator_type &alloc, Util::DebugLog *debugLog, bool poolCached);
	~btree_mempool() {
		if (dtranxHelper_ != NULL) {
			delete dtranxHelper_;
		}
		if (keySpace_ != NULL) {
			delete keySpace_;
		}
	}
	void InitDTranxForBTree(Util::DTranxHelper *dtranxHelper) {
		assert(dtranxHelper_ == NULL);
		dtranxHelper_ = dtranxHelper;
		keySpace_ = new Util::DTranxKeySpace(dtranxHelper_->ReadMeta());
	}
	/*
	 * Access Interface
	 */
	node_type *GetNode(std::string key, bool& alreadyRead);
	node_type *GetRoot();
	/*
	 * AddWrite add node to the writeset during commit
	 * 	the first AddWrite only add new allocated nodes
	 * 	the second AddWrite adds deleted nodes
	 *
	 */
	void AddWrite(std::string key, node_type *newNode);
	void AddWrite(std::string key);

	std::string GetNextNewKey() {
		return keySpace_->GetNextNewKey();
	}
public:
	/*
	 * Node creation/deletion routines.
	 */
	node_type* new_node() {
		return new node_type();
	}
	node_type* new_internal_node() {
		node_type* node = new node_type();
		node->set_selfkey(keySpace_->GetNextNewKey());
		AddWrite(node->selfkey(), node);
		return node;
	}
	node_type* new_internal_root_node() {
		node_type* node = new node_type();
		AddWrite(Util::DTranxKeySpace::ROOTNODEKEY, node);
		return node;
	}
	node_type* new_leaf_node() {
		node_type* node = new node_type();
		node->set_selfkey(keySpace_->GetNextNewKey());
		AddWrite(node->selfkey(), node);
		return node;
	}
	node_type* new_leaf_root_node() {
		node_type* node = new node_type();
		node->set_selfkey(Util::DTranxKeySpace::ROOTNODEKEY);
		AddWrite(Util::DTranxKeySpace::ROOTNODEKEY, node);
		return node;
	}
	void delete_internal_node(node_type *node) {
		AddWrite(node->selfkey());
	}
	void delete_internal_root_node(node_type *node) {
		AddWrite(node->selfkey());
	}
	void delete_leaf_node(node_type *node) {
		AddWrite(node->selfkey());
	}

	/*
	 * Transaction interface
	 */

	void ClearNodeInPool(std::unordered_map<std::string, node_type*>& pool, std::string key) {
		if (pool.find(key) != pool.end() && pool[key] != NULL) {
			delete pool[key];
		}
		pool.erase(key);
	}

	void Clear(bool commitSucc) {
		if (isPoolCached) {
			if (commitSucc) {
				node_type *temp;
				for (auto it = swapSet.begin(); it != swapSet.end(); ++it) {
					//assert(nodesPool_.find(*it) != nodesPool_.end());
					std::string key1 = *it;
					std::string key2 = nodesPool_[*it]->selfkey();
					if (key1 == key2) {
						continue;
					}
					//assert(nodesPool_.find(key2) != nodesPool_.end());
					temp = nodesPool_[key1];
					nodesPool_[key1] = nodesPool_[key2];
					nodesPool_[key2] = temp;
					if (nodesPool_original_read.find(key1) == nodesPool_original_read.end()) {
						nodesPool_original_read[key1] = new_node();
					}
					nodesPool_[key1]->CopyNode(nodesPool_original_read[key1]);
					if (nodesPool_original_read.find(key2) == nodesPool_original_read.end()) {
						nodesPool_original_read[key2] = new_node();
					}
					nodesPool_[key2]->CopyNode(nodesPool_original_read[key2]);
				}
				for (auto it = writeSet.begin(); it != writeSet.end(); ++it) {
					if (swapSet.find(*it) != swapSet.end()) {
						continue;
					}
					//assert(nodesPool_.find(*it) != nodesPool_.end());
					if (nodesPool_original_read.find(*it) == nodesPool_original_read.end()) {
						/*
						 * special case, for new nodes, erase the cache, because
						 * if any concurrent transaction updated this new node,
						 * this outdated in-mem version is used but check will succeed since
						 * DDSBrick cache has the right version number.
						 */
						if (nodesPool_[*it] != NULL) {
							delete nodesPool_[*it];
							nodesPool_.erase(*it);
						}
						continue;

					}
					nodesPool_[*it]->CopyNode(nodesPool_original_read[*it]);
					nodesPool_[*it]->ResetPointerKey();
					nodesPool_original_read[*it]->ResetPointerKey();
				}
				for (auto it = updateSet.begin(); it != updateSet.end(); ++it) {
					//assert(nodesPool_.find(*it) != nodesPool_.end());
					//assert(nodesPool_original_read.find(*it) != nodesPool_original_read.end());
					nodesPool_[*it]->CopyNode(nodesPool_original_read[*it]);
					nodesPool_[*it]->ResetPointerKey();
					nodesPool_original_read[*it]->ResetPointerKey();
				}
				for (auto it = readSet.begin(); it != readSet.end(); ++it) {
					nodesPool_[*it]->ResetPointerKey();
					nodesPool_original_read[*it]->ResetPointerKey();
				}
				for (auto it = swapSet.begin(); it != swapSet.end(); ++it) {
					nodesPool_[*it]->ResetPointerKey();
					nodesPool_original_read[*it]->ResetPointerKey();
				}
			} else {
				for (auto it = readSet.begin(); it != readSet.end(); ++it) {
					ClearNodeInPool(nodesPool_, *it);
					ClearNodeInPool(nodesPool_original_read, *it);
				}
				for (auto it = writeSet.begin(); it != writeSet.end(); ++it) {
					ClearNodeInPool(nodesPool_, *it);
					ClearNodeInPool(nodesPool_original_read, *it);
				}
				for (auto it = swapSet.begin(); it != swapSet.end(); ++it) {
					ClearNodeInPool(nodesPool_, *it);
					ClearNodeInPool(nodesPool_original_read, *it);
				}
				for (auto it = updateSet.begin(); it != updateSet.end(); ++it) {
					ClearNodeInPool(nodesPool_, *it);
					ClearNodeInPool(nodesPool_original_read, *it);
				}
			}
			readSet.clear();
			writeSet.clear();
			swapSet.clear();
			updateSet.clear();
		} else {
			for (auto it = nodesPool_.begin(); it != nodesPool_.end(); ++it) {
				if (it->second != NULL) {
					delete it->second;
				}
			}
			for (auto it = nodesPool_original_read.begin(); it != nodesPool_original_read.end();
					++it) {
				if (it->second != NULL) {
					delete it->second;
				}
			}
			nodesPool_.clear();
			nodesPool_original_read.clear();
			readSet.clear();
			writeSet.clear();
			swapSet.clear();
			updateSet.clear();
		}
	}

	void Abort(std::string message) {
		debugLog->Write(Util::ABORT_PRINT_START);
		debugLog->Write(message);
		debugLog->Write(Util::ABORT_PRINT_END);
		dtranxHelper_->Abort();
		Clear(false);
	}

	bool Commit();

private:
	Util::DTranxHelper *dtranxHelper_;
	internal_allocator_type alloc_;
	/*
	 * nodesPool_* is a memory pool that stores all de-serialized node objects
	 * specifically nodesPool_ is the one that might be changed during the transaction
	 * while nodesPool_original_read only records the version that is read in
	 * the first place. nodesPool_original_read is used to detect writeset
	 * during transaction commit
	 *
	 *	writeSet: new/delete nodes, might include update nodes
	 *		e.g. when new root is created, old root is automatically added to writeSet by AddWrite func.
	 *	swapSet: swapped nodes
	 *	updateSet: updated pre-existing nodes
	 *	readSet: read items
	 */
	std::unordered_map<std::string, node_type *> nodesPool_;
	std::unordered_map<std::string, node_type *> nodesPool_original_read;
	std::unordered_set<std::string> writeSet;
	std::unordered_set<std::string> swapSet;
	std::unordered_set<std::string> updateSet;
	std::unordered_set<std::string> readSet;

	bool isPoolCached;

	/*
	 * keySpace helps to generate next new key
	 */
	Util::DTranxKeySpace *keySpace_;

	Util::DebugLog *debugLog;
private:
	/*
	 * Allocator routines.
	 */
	internal_allocator_type* mutable_internal_allocator() {
		return static_cast<internal_allocator_type*>(&alloc_);
	}
	const internal_allocator_type& internal_allocator() const {
		return *static_cast<const internal_allocator_type*>(&alloc_);
	}

};

template<typename P>
btree_mempool<P>::btree_mempool(const allocator_type &alloc, Util::DebugLog *debugLog,
bool poolCached)
		: dtranxHelper_(NULL), keySpace_(NULL), alloc_(alloc), debugLog(debugLog), isPoolCached(
				poolCached) {
	std::cout << "btree_mempool constructor" << std::endl;
}

template<typename P>
btree_node<P> *btree_mempool<P>::GetRoot() {
	if (nodesPool_.find(Util::DTranxKeySpace::ROOTNODEKEY) != nodesPool_.end()) {
		if (readSet.find(Util::DTranxKeySpace::ROOTNODEKEY) == readSet.end()) {
			readSet.insert(Util::DTranxKeySpace::ROOTNODEKEY);
			dtranxHelper_->ReadNode(Util::DTranxKeySpace::ROOTNODEKEY);
		}
		return nodesPool_[Util::DTranxKeySpace::ROOTNODEKEY];
	}
	std::string content = dtranxHelper_->ReadNode(Util::DTranxKeySpace::ROOTNODEKEY);
	if (content == Util::DTranxKeySpace::NOSUCHNODE) {
		return NULL;
	}
	readSet.insert(Util::DTranxKeySpace::ROOTNODEKEY);
	btree_node<P> *p = new_node();
	btree_node<P> *q = new_node();
	p->deserialize(content, Util::DTranxKeySpace::ROOTNODEKEY);
	nodesPool_[Util::DTranxKeySpace::ROOTNODEKEY] = p;
	p->CopyNode(q);
	nodesPool_original_read[Util::DTranxKeySpace::ROOTNODEKEY] = q;
	if (debugLog->isDebugEnabled()) {
		debugLog->Write(p->Print());
	}
	return p;
}

template<typename P>
btree_node<P> *btree_mempool<P>::GetNode(std::string key, bool& alreadyRead) {
	alreadyRead = false;
	if (nodesPool_.find(key) != nodesPool_.end()) {
		if (readSet.find(key) == readSet.end()) {
			readSet.insert(key);
			dtranxHelper_->ReadNode(key);
		} else {
			alreadyRead = true;
		}
		return nodesPool_[key];
	}
	std::string content = dtranxHelper_->ReadNode(key);
	if (content == Util::DTranxKeySpace::NOSUCHNODE) {
		return NULL;
	}
	readSet.insert(key);
	btree_node<P> *p = new_node();
	btree_node<P> *q = new_node();
	p->deserialize(content, key);
	nodesPool_[key] = p;
	p->CopyNode(q);
	nodesPool_original_read[key] = q;
	if (debugLog->isDebugEnabled()) {
		debugLog->Write(p->Print());
	}
	return p;
}

template<typename P>
void btree_mempool<P>::AddWrite(std::string key, btree_node<P> *newNode) {
	/*
	 * it might already be in the pool
	 * eg. new root is created
	 * make sure the previous node is already configured with another new key
	 * besides, that new key should be generated recently and not in the dtranx db yet.
	 */
	if (nodesPool_.find(key) != nodesPool_.end()) {
		//assert(key != nodesPool_[key]->selfkey());
		std::string newKey = nodesPool_[key]->selfkey();
		//assert(nodesPool_.find(newKey) == nodesPool_.end());
		nodesPool_[newKey] = nodesPool_[key];
		writeSet.insert(newKey);
		nodesPool_.erase(key);
	}
	nodesPool_[key] = newNode;
	writeSet.insert(key);
}

template<typename P>
void btree_mempool<P>::AddWrite(std::string key) {
	//assert(nodesPool_.find(key) != nodesPool_.end());
	writeSet.insert(key);
}

template<typename P>
bool btree_mempool<P>::Commit() {
	bool readTranx = true;
	debugLog->Write(Util::COMMIT_PRINT_START);
	/*
	 * Find all the swapping
	 */
	for (auto it = readSet.begin(); it != readSet.end(); ++it) {
		if (*it != nodesPool_[*it]->selfkey()) {
			swapSet.insert(nodesPool_[*it]->selfkey());
			readTranx = false;
			dtranxHelper_->WriteNode(nodesPool_[*it]->selfkey(), nodesPool_[*it]->serialize());
			if (debugLog->isDebugEnabled()) {
				debugLog->Write(nodesPool_[*it]->Print());
			}
		}
	}
	/*
	 * Add new allocated nodes to writeset
	 */
	for (auto it = writeSet.begin(); it != writeSet.end(); ++it) {
		readTranx = false;
		dtranxHelper_->WriteNode(*it, nodesPool_[*it]->serialize());
		if (debugLog->isDebugEnabled()) {
			debugLog->Write(nodesPool_[*it]->Print());
		}
	}
	/*
	 * Automatically detect any modified nodes
	 */
	for (auto it = readSet.begin(); it != readSet.end(); ++it) {
		if (writeSet.find(*it) != writeSet.end()) {
			continue;
		}
		if (swapSet.find(*it) != swapSet.end()) {
			continue;
		}
		//assert(nodesPool_original_read.find(*it) != nodesPool_original_read.end());
		if (nodesPool_[*it]->IsNodeDifferent(nodesPool_original_read[*it])) {
			updateSet.insert(*it);
			readTranx = false;
			dtranxHelper_->WriteNode(*it, nodesPool_[*it]->serialize());
			if (debugLog->isDebugEnabled()) {
				debugLog->Write(nodesPool_[*it]->Print());
			}
		}
	}
	debugLog->Write(Util::COMMIT_PRINT_END);
	bool result = dtranxHelper_->Commit();
	Clear(result);
	return result;
}

}  // namespace Core
}  // namespace BTree

#endif
