/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef BTREE_CORE_BTREE_HELPER_H_
#define BTREE_CORE_BTREE_HELPER_H_

#include <algorithm>
#include "BTree/Core/btree_include.h"

#ifndef NDEBUG
#define NDEBUG 0
#endif

namespace BTree {
namespace Core {

// Inside a btree method, if we just call swap(), it will choose the
// btree::swap method, which we don't want. And we can't say ::swap
// because then MSVC won't pickup any std::swap() implementations. We
// can't just use std::swap() directly because then we don't get the
// specialization for types outside the std namespace. So the solution
// is to have a special swap helper function whose name doesn't
// collide with other swap functions defined by the btree classes.
template<typename T>
inline void btree_swap_helper(T &a, T &b) {
	using std::swap;
	swap(a, b);
}

// A template helper used to select A or B based on a condition.
template<bool cond, typename A, typename B>
struct if_ {
	typedef A type;
};

template<typename A, typename B>
struct if_<false, A, B> {
	typedef B type;
};

// Types small_ and big_ are promise that sizeof(small_) < sizeof(big_)
typedef char small_;

struct big_ {
	char dummy[2];
};

// A compile-time assertion.
template<bool>
struct CompileAssert {
};

#define COMPILE_ASSERT(expr, msg) \
  typedef CompileAssert<(bool(expr))> msg[bool(expr) ? 1 : -1]

// A helper type used to indicate that a key-compare-to functor has been
// provided. A user can specify a key-compare-to functor by doing:
//
//  struct MyStringComparer
//      : public util::btree::btree_key_compare_to_tag {
//    int operator()(const string &a, const string &b) const {
//      return a.compare(b);
//    }
//  };
//
// Note that the return type is an int and not a bool. There is a
// COMPILE_ASSERT which enforces this return type.
struct btree_key_compare_to_tag {
};

// A helper class that indicates if the Compare parameter is derived from
// btree_key_compare_to_tag.
template<typename Compare>
struct btree_is_key_compare_to: public std::is_convertible<Compare, btree_key_compare_to_tag> {
};

// A helper class to convert a boolean comparison into a three-way
// "compare-to" comparison that returns a negative value to indicate
// less-than, zero to indicate equality and a positive value to
// indicate greater-than. This helper class is specialized for
// less<string> and greater<string>. The btree_key_compare_to_adapter
// class is provided so that btree users automatically get the more
// efficient compare-to code when using common google string types
// with common comparison functors.
template<typename Compare>
struct btree_key_compare_to_adapter: Compare {
	btree_key_compare_to_adapter() {
	}
	btree_key_compare_to_adapter(const Compare &c)
			: Compare(c) {
	}
	btree_key_compare_to_adapter(const btree_key_compare_to_adapter<Compare> &c)
			: Compare(c) {
	}
};

template<>
struct btree_key_compare_to_adapter<std::less<std::string> > : public btree_key_compare_to_tag {
	btree_key_compare_to_adapter() {
	}
	btree_key_compare_to_adapter(const std::less<std::string>&) {
	}
	btree_key_compare_to_adapter(const btree_key_compare_to_adapter<std::less<std::string> >&) {
	}
	int operator()(const std::string &a, const std::string &b) const {
		return a.compare(b);
	}
};

template<>
struct btree_key_compare_to_adapter<std::greater<std::string> > : public btree_key_compare_to_tag {
	btree_key_compare_to_adapter() {
	}
	btree_key_compare_to_adapter(const std::greater<std::string>&) {
	}
	btree_key_compare_to_adapter(const btree_key_compare_to_adapter<std::greater<std::string> >&) {
	}
	int operator()(const std::string &a, const std::string &b) const {
		return b.compare(a);
	}
};

// A helper class that allows a compare-to functor to behave like a plain
// compare functor. This specialization is used when we do not have a
// compare-to functor.
template<typename Key, typename Compare, bool HaveCompareTo>
struct btree_key_comparer {
	btree_key_comparer() {
	}
	btree_key_comparer(Compare c)
			: comp(c) {
	}
	static bool bool_compare(const Compare &comp, const Key &x, const Key &y) {
		return comp(x, y);
	}
	bool operator()(const Key &x, const Key &y) const {
		return bool_compare(comp, x, y);
	}
	Compare comp;
};

// A specialization of btree_key_comparer when a compare-to functor is
// present. We need a plain (boolean) comparison in some parts of the btree
// code, such as insert-with-hint.
template<typename Key, typename Compare>
struct btree_key_comparer<Key, Compare, true> {
	btree_key_comparer() {
	}
	btree_key_comparer(Compare c)
			: comp(c) {
	}
	static bool bool_compare(const Compare &comp, const Key &x, const Key &y) {
		return comp(x, y) < 0;
	}
	bool operator()(const Key &x, const Key &y) const {
		return bool_compare(comp, x, y);
	}
	Compare comp;
};

// A helper function to compare to keys using the specified compare
// functor. This dispatches to the appropriate btree_key_comparer comparison,
// depending on whether we have a compare-to functor or not (which depends on
// whether Compare is derived from btree_key_compare_to_tag).
template<typename Key, typename Compare>
static bool btree_compare_keys(const Compare &comp, const Key &x, const Key &y) {
	typedef btree_key_comparer<Key, Compare, btree_is_key_compare_to<Compare>::value> key_comparer;
	return key_comparer::bool_compare(comp, x, y);
}

template<typename Key, typename Compare, typename Alloc, int TargetNodeSize, int ValueSize>
struct btree_common_params {
	// If Compare is derived from btree_key_compare_to_tag then use it as the
	// key_compare type. Otherwise, use btree_key_compare_to_adapter<> which will
	// fall-back to Compare if we don't have an appropriate specialization.
	typedef typename if_<btree_is_key_compare_to<Compare>::value, Compare,
			btree_key_compare_to_adapter<Compare> >::type key_compare;
	// A type which indicates if we have a key-compare-to functor or a plain old
	// key-compare functor.
	typedef btree_is_key_compare_to<key_compare> is_key_compare_to;

	typedef Alloc allocator_type;
	typedef Key key_type;
	typedef ssize_t size_type;
	typedef ptrdiff_t difference_type;

	enum {
		kTargetNodeSize = TargetNodeSize,

		// Available space for values.  This is largest for leaf nodes,
		// which has overhead no fewer than two pointers.
		kNodeValueSpace = TargetNodeSize - 2 * sizeof(void*),
	};

	// This is an integral type large enough to hold as many
	// ValueSize-values as will fit a node of TargetNodeSize bytes.
	typedef typename if_<(kNodeValueSpace / ValueSize) >= 256, uint16_t, uint8_t>::type node_count_type;
};

// A parameters structure for holding the type parameters for a btree_map.
template<typename Key, typename Data, typename Compare, typename Alloc, int TargetNodeSize>
struct btree_map_params: public btree_common_params<Key, Compare, Alloc, TargetNodeSize,
		sizeof(Key) + sizeof(Data)> {
	typedef Data data_type;
	typedef Data mapped_type;
	typedef std::pair<const Key, data_type> value_type;
	typedef std::pair<Key, data_type> mutable_value_type;
	typedef value_type* pointer;
	typedef const value_type* const_pointer;
	typedef value_type& reference;
	typedef const value_type& const_reference;

	enum {
		kValueSize = sizeof(Key) + sizeof(data_type),
	};

	static const Key& key(const value_type &x) {
		return x.first;
	}
	static const Key& key(const mutable_value_type &x) {
		return x.first;
	}
	static void swap(mutable_value_type *a, mutable_value_type *b) {
		btree_swap_helper(a->first, b->first);
		btree_swap_helper(a->second, b->second);
	}
};

// A parameters structure for holding the type parameters for a btree_set.
template<typename Key, typename Compare, typename Alloc, int TargetNodeSize>
struct btree_set_params: public btree_common_params<Key, Compare, Alloc, TargetNodeSize, sizeof(Key)> {
	typedef std::false_type data_type;
	typedef std::false_type mapped_type;
	typedef Key value_type;
	typedef value_type mutable_value_type;
	typedef value_type* pointer;
	typedef const value_type* const_pointer;
	typedef value_type& reference;
	typedef const value_type& const_reference;

	enum {
		kValueSize = sizeof(Key),
	};

	static const Key& key(const value_type &x) {
		return x;
	}
	static void swap(mutable_value_type *a, mutable_value_type *b) {
		btree_swap_helper<mutable_value_type>(*a, *b);
	}
};

// An adapter class that converts a lower-bound compare into an upper-bound
// compare.
template<typename Key, typename Compare>
struct btree_upper_bound_adapter: public Compare {
	btree_upper_bound_adapter(Compare c)
			: Compare(c) {
	}
	bool operator()(const Key &a, const Key &b) const {
		return !static_cast<const Compare&>(*this)(b, a);
	}
};

template<typename Key, typename CompareTo>
struct btree_upper_bound_compare_to_adapter: public CompareTo {
	btree_upper_bound_compare_to_adapter(CompareTo c)
			: CompareTo(c) {
	}
	int operator()(const Key &a, const Key &b) const {
		return static_cast<const CompareTo&>(*this)(b, a);
	}
};

// Dispatch helper class for using linear search with plain compare.
template<typename K, typename N, typename Compare>
struct btree_linear_search_plain_compare {
	static int lower_bound(const K &k, const N &n, Compare comp) {
		return n.linear_search_plain_compare(k, 0, n.count(), comp);
	}
	static int upper_bound(const K &k, const N &n, Compare comp) {
		typedef btree_upper_bound_adapter<K, Compare> upper_compare;
		return n.linear_search_plain_compare(k, 0, n.count(), upper_compare(comp));
	}
};

// Dispatch helper class for using linear search with compare-to
template<typename K, typename N, typename CompareTo>
struct btree_linear_search_compare_to {
	static int lower_bound(const K &k, const N &n, CompareTo comp) {
		return n.linear_search_compare_to(k, 0, n.count(), comp);
	}
	static int upper_bound(const K &k, const N &n, CompareTo comp) {
		typedef btree_upper_bound_adapter<K, btree_key_comparer<K, CompareTo, true> > upper_compare;
		return n.linear_search_plain_compare(k, 0, n.count(), upper_compare(comp));
	}
};

// Dispatch helper class for using binary search with plain compare.
template<typename K, typename N, typename Compare>
struct btree_binary_search_plain_compare {
	static int lower_bound(const K &k, const N &n, Compare comp) {
		return n.binary_search_plain_compare(k, 0, n.count(), comp);
	}
	static int upper_bound(const K &k, const N &n, Compare comp) {
		typedef btree_upper_bound_adapter<K, Compare> upper_compare;
		return n.binary_search_plain_compare(k, 0, n.count(), upper_compare(comp));
	}
};

// Dispatch helper class for using binary search with compare-to.
template<typename K, typename N, typename CompareTo>
struct btree_binary_search_compare_to {
	static int lower_bound(const K &k, const N &n, CompareTo comp) {
		return n.binary_search_compare_to(k, 0, n.count(), CompareTo());
	}
	static int upper_bound(const K &k, const N &n, CompareTo comp) {
		typedef btree_upper_bound_adapter<K, btree_key_comparer<K, CompareTo, true> > upper_compare;
		return n.linear_search_plain_compare(k, 0, n.count(), upper_compare(comp));
	}
};

}// namespace Core
}// namespace BTree
#endif /* BTREE_CORE_BTREE_HELPER_H_ */
