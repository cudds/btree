/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * DtranxHelper helps to talk to the DTranx service
 *
 * METANODEKEY stores client ID's, be sure to create a clientid in the database
 * during initialization to avoid contention bug/blind write on METANODEKEY
 *
 * not threadsafe
 */

#ifndef BTREE_UTIL_DTRANXHELPER_H_
#define BTREE_UTIL_DTRANXHELPER_H_

#include "DTranx/Client/ClientTranx.h"

namespace BTree {
namespace Util {

class DTranxHelper {
	//MaybeLater: support delete key values
public:
	DTranxHelper(uint32_t routerFrontPort,
			std::vector<std::string> ips,
			std::string selfAddress,
			uint32_t selfPort,
			bool clientCacheEnabled = true, bool snapshotTranx = false);
	~DTranxHelper();
	void InitClient(std::string ip, DTranx::Client::Client* client);

	bool Commit();
	void Abort();

	std::string ReadNode(std::string nodeID);
	void WriteNode(std::string nodeID, std::string content);
	/*
	 * InitMeta is called when BTree is first created upon
	 * DTranx instance
	 */
	void InitMeta();
	uint64_t ReadMeta();
private:
	DTranx::Client::ClientTranx *clientTranx;
	bool snapshotTranx;
};

}  // namespace Util
}  // namespace BTree
#endif
