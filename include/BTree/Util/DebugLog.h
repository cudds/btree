/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * DebugLog outputs the debug log.
 * Not thread safe, synchronous write
 */

#ifndef DTRANX_DEBUGLOG_CC_
#define DTRANX_DEBUGLOG_CC_

#include <iostream>

namespace BTree {
namespace Util {

/*
 * These const string are printed for specific debug messages
 * it's mainly used for post processing
 *
 * NODE_PRINT_START/NODE_PRINT_END
 * 	used to print btree nodes
 *
 * COMMIT_PRINT_START/COMMIT_PRINT_END
 * 	used to print commit information, called at btree mempool commit function
 *
 * ABORT_PRINT_START/ABORT_PRINT_END
 * 	used to print abort information, called at btree mempool abort function
 *
 * DTRANX_COMMIT/DTRANX_ABORT
 * 	used to print whether commit succ/fail, called at btree insert/find/erase functions
 *
 * IMPORTANT: If any of these printing strategy changes, please go to the Scripts to adjust
 * for the changes for post processing
 *
 */
const std::string NODE_PRINT_START = "NODEPRINTSTART\n";
const std::string NODE_PRINT_END = "NODEPRINTEND\n";
const std::string COMMIT_PRINT_START = "COMMITPRINTSTART\n";
const std::string COMMIT_PRINT_END = "COMMITPRINTEND\n";
const std::string ABORT_PRINT_START = "ABORTPRINTSTART\n";
const std::string ABORT_PRINT_END = "ABORTPRINTEND\n";
const std::string DTRANX_COMMIT = "DTRANXCOMMIT\n";
const std::string DTRANX_ABORT  = "DTRANXABORT\n";

/*
 * Now debugLog is written as follows
 * 	1. read node-> print node
 * 	2. commit tranx-> print commit and writeset
 * 	3. abort tranx-> print abort and show abort message
 * 	3. print whether commit success at DTranx servers
 */

class DebugLog {
public:
	DebugLog()
			: file_() {
		enableDebug = false;
	}
	DebugLog(bool enableDebug, std::string fileName);

	~DebugLog();

	bool isDebugEnabled() {
		return enableDebug;
	}

	void DisableDebug() {
		enableDebug = false;
	}

	void EnableDebug() {
		enableDebug = true;
	}
	void Write(std::string data);
	void Flush();
private:
	/*
	 * fileName is the file name to output the debug information to.
	 */
	bool enableDebug;
	std::string fileName;
	FILE* file_;
};
}
}

#endif /* DTRANX_DEBUGLOG_CC_ */
