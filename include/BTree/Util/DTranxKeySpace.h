/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef BTREE_DTRANXKEYSPACE_H_
#define BTREE_DTRANXKEYSPACE_H_

#include <string>
namespace BTree {
namespace Util {
class DTranxKeySpace {
public:
	DTranxKeySpace(uint64_t clientID);
	static std::string NOSUCHNODE;
	static std::string METANODEKEY;
	static std::string ROOTNODEKEY;
	static char DELIMITER;

	std::string GetNextNewKey();
	uint64_t clientID_;
	uint64_t nextID_;
};
}
}

#endif /* BTREE_DTRANXKEYSPACE_H_ */
