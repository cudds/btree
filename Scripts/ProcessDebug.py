import glob
import re
from Node import Node
from Transaction import Transaction

#log related strings
NODE_PRINT_START = "NODEPRINTSTART"
NODE_PRINT_END = "NODEPRINTEND"
COMMIT_PRINT_START = "COMMITPRINTSTART"
COMMIT_PRINT_END = "COMMITPRINTEND"
ABORT_PRINT_START = "ABORTPRINTSTART"
ABORT_PRINT_END = "ABORTPRINTEND"
DTRANX_COMMIT = "DTRANXCOMMIT"
DTRANX_ABORT  = "DTRANXABORT"

class FileReadClass:
	def __init__(self, fileName):
		self.file = open(fileName, 'r')
		self.lineNum = 0
	def Close(self):
		self.file.close()
	def ReadNext(self):
		self.lineNum += 1
		return self.file.readline(), self.lineNum
	def Skip(self, numOfLines):
		for i in range(numOfLines):
			self.lineNum += 1
			self.file.readline()
#
# ReadNode reads from the file and processed as a Node object
# 	note: the NODE_PRINT_START is already read
# 	@return Node object, if the object is not complete, return None
def ReadNode(fHandler):
	node = Node()
	# read self key
	fHandler.Skip(1)
	line, lineNum = fHandler.ReadNext()
	if not line:
		return None
	if len(line.strip().split(' ')) < 2:
		return None
	node.SetSelfKey(line.strip().split(' ')[1])

	# read position
	fHandler.Skip(1)
	line, lineNum = fHandler.ReadNext()
	if not line:
		return None
	if len(line.strip().split(' ')) < 2:
		return None
	node.SetPosition(int(line.strip().split(' ')[1]))

	# read parent key
	fHandler.Skip(2)
	line, lineNum = fHandler.ReadNext()
	if not line:
		return None
	if len(line.strip().split(' ')) < 2:
		return None
	node.SetParentKey(line.strip().split(' ')[1])

	# read children
	fHandler.Skip(3)
	line, lineNum = fHandler.ReadNext()
	if not line:
		return None
	if len(line.strip().split(' ')) != 30:
		return None
	children = line.strip().split(' ')
	for child in children:
		node.AddChild(child)
	fHandler.Skip(2)
	return node

#
# ReadTransaction reads from the file and processed as a Transaction object
#	@return Transaction object, if the object is not complete, return None
#
# TODO: move the following verbose codes into this function
def ReadTransaction(fHandler):
	pass

#
# btree.debug* is named in YCSB program
# so if that changes, this might need to change too
#
# tranxs is a global variable storing all transactions
#
fileNames = glob.glob("./btree.debug*")
tranxs = []
# process each debug log
for fileName in fileNames:
	print "examing ", fileName
	fHandler = FileReadClass(fileName)
	line, lineNum = fHandler.ReadNext()
	# tranxStart denotes whether to create a new Transaction object
	tranxStart = True
	tranx = Transaction()
	while line:
		if re.match(NODE_PRINT_START, line):
			if tranxStart:
				tranxStart = False
				tranx = Transaction()
				tranx.SetLineNum(lineNum)
			node = ReadNode(fHandler)
			if node is None:
				break
			tranx.AddRead(node)
			line, lineNum = fHandler.ReadNext()
		elif re.match(COMMIT_PRINT_START, line):
			toBreak = False
			while not re.match(COMMIT_PRINT_END, line):
				line, lineNum = fHandler.ReadNext()
				if re.match(NODE_PRINT_START, line):
					node = ReadNode(fHandler)
					if node is None:
						toBreak = True
						break
					tranx.AddWrite(node)
			if toBreak:
				break
			line, lineNum = fHandler.ReadNext()
			if re.match(DTRANX_COMMIT, line):
				tranx.SetCommit()
			elif re.match(DTRANX_ABORT, line):
				tranx.SetAbort()
			else:
				break
			tranxs.append(tranx)
			line, lineNum = fHandler.ReadNext()
			tranxStart = True
		elif re.match(ABORT_PRINT_START, line):
			while not re.match(ABORT_PRINT_END, line):
				line, lineNum = fHandler.ReadNext()
			tranx.SetAbort()
			tranxs.append(tranx)
			line, lineNum = fHandler.ReadNext()
			tranxStart = True
		else:
			break
	fHandler.Close()
	for tranx in tranxs:
		'''
		if tranx.IsCommit() and tranx.CheckInWrites("1-1652") and tranx.GetWriteNode("1-1652").GetParentKey() == "33-1766":
			if tranx.CheckInReads("1-1652") and tranx.GetReadNode("1-1652").GetParentKey() != "33-1766":
				tranx.Print()
		'''
		'''
		if tranx.IsCommit() and tranx.CheckInWrites("19-796") and tranx.GetWriteNode("19-796").CheckInChildren("1-1652"):
			if tranx.CheckInReads("19-796") and not tranx.GetReadNode("19-796").CheckInChildren("1-1652"):
				tranx.Print()
		'''
		'''
		if tranx.IsCommit() and tranx.CheckInReads("1-1652") and tranx.GetReadNode("1-1652").GetParentKey() == "19-796":
			tranx.Print()
		'''
		if tranx.IsCommit() and tranx.CheckInWrites("1-1652") and tranx.GetWriteNode("1-1652").GetParentKey() == "33-1766":
			if not tranx.CheckInReads("1-1652"):
				tranx.Print()
	del tranxs[:]
