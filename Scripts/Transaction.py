from Node import Node
# Transaction represent a transaction class
class Transaction:
	def __init__(self):
		self.commit = True
		self.readset = {}
		self.writeset= {}
		self.linenum = 0
	def AddRead(self, readNode):
		self.readset[readNode.GetSelfKey()] = readNode
	def CheckInReads(self, key):
		if self.readset.has_key(key):
			return True
		return False
	def GetReadNode(self, key):
		return self.readset[key]
	def AddWrite(self, writeNode):
		self.writeset[writeNode.GetSelfKey()] = writeNode
	def CheckInWrites(self, key):
		if self.writeset.has_key(key):
			return True
		return False
	def GetWriteNode(self, key):
		return self.writeset[key]
	def SetCommit(self):
		self.commit = True
	def IsCommit(self):
		return self.commit
	def SetAbort(self):
		self.commit = False
	def SetLineNum(self, lineNum):
		self.linenum = lineNum
	def Print(self):
		printStr = "line num: " + str(self.linenum) + "\n"
		printStr += "readset: "
		for readKey in self.readset.keys():
			printStr += readKey + " "
		printStr += "\n"
		printStr += "writeset: "
		for writeKey in self.writeset.keys():
			printStr += writeKey + " "
		printStr += "\n"
		if self.commit:
			printStr += "commit"
		else:
			printStr += "abort"
		print printStr