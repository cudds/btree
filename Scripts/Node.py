class Node:
	def __init__(self):
		self.selfkey = ""
		self.position = -1
		self.parentkey = ""
		self.children = []
	def SetSelfKey(self, selfkey):
		self.selfkey = selfkey
	def GetSelfKey(self):
		return self.selfkey
	def SetPosition(self, position):
		self.position = position
	def GetPosition(self):
		return self.position
	def SetParentKey(self, parentkey):
		self.parentkey = parentkey
	def GetParentKey(self):
		return self.parentkey
	def AddChild(self, child):
		self.children.append(child)
	def GetChild(self, index):
		if index < len(children):
			return self.children[index]
		return -1
	def CheckInChildren(self, key):
		if key in self.children:
			return True
		return False
	def Print(self):
		print "selfkey: ", self.selfkey
		print "position: ", self.position
		print "parentkey: ", self.parentkey
		childrenStr = ""
		for child in self.children:
			childrenStr += child + ","
		print "children: ", childrenStr